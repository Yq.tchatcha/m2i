import datetime


class Voiture:
    def __init__(self):
        self.modele = "Mondeo"
        self.immatriculation = "000XX00"
        self.dateCirculation = "10-09-2000"

    def get_Modele(self):
        return self.modele

    def get_Immatriculation(self):
        return self.immatriculation

    def get_DateCirculation(self):
        return self.dateCirculation

    def set_Modele(self,modele):
        self.modele = modele

    def set_Immatriculation(self,immatriculation):
        self.immatriculation = immatriculation

    def set_DateCirculation(self,dateCirculation):
        self.dateCirculation = dateCirculation

    def __str__(self):
        return "Modele: " + self.modele + " immatriculation: " + self.immat + " date: " + self.dateCirculation


    def estfr(self):
        if self.immatriculation[::2] == [0-9] and not self.immatriculation.isdigit() and self.immatriculation[5:6] == [0-9]:
            print("L'immatricule ",self.immatriculation, " est francaise")
        else:
            print("L'immatricule ", self.immatriculation, " n'est pas francaise")

    def estDoCollection(self):
        dateDeSortie = datetime.datetime.now()
        if (dateDeSortie.year - 20) > self.dateCirculation[6-9]:
            print("Il s'agit d'un vehicule de collection")
        else:
            print("Il ne s'agit pas d'un vehicule de collection")

    def estDeCollection2(self):
        now = datetime.datetime.now()
        if (now.year - 20) >= int(self.dateCirculation[6:11]):
            return True
        return False
