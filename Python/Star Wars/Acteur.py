class Acteur:
    def __init__(self,nom,prenom,listePersonnage):
        self.nom = nom
        self.prenom = prenom
        self.listePersonnage = listePersonnage #7


    def get_Nom(self):
        return self.nom

    def set_Nom(self,nom):
        self.nom = nom

    def get_Prenom(self):
        return self.prenom

    def set_Prenom(self,prenom):
        self.prenom = prenom

    def __str__(self):
        return "Acteur : " + self.nom +  " Prenom: " + self.prenom

    # retourne le nombre de personnages incarné par cet acteur
    def nbPersonnages(self):
        return len(self.listePersonnage)



