class Personnage:
    def __init__(self,nom,prenom):
        self.nom = nom
        self.prenom = prenom

    def get_Nom(self):
        return self.nom

    def set_Nom(self,nom):
        self.nom = nom

    def get_Prenom(self):
        return self.prenom

    def set_Prenom(self,prenom):
        self.prenom = prenom

    def __str__(self):
        return "Personnage : " + self.nom +  " Prenom: " + self.prenom
