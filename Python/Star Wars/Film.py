class Film:
    def __init__(self,titre,anneeDeSortie,numeroEpisode,cout,recette,listActeur,listePersonnage):
        self.titre = titre
        self.anneeDeSortie = anneeDeSortie
        self.numeroEpisode = numeroEpisode
        self.cout = cout
        self.recette = recette
        self.listActeur = listActeur #7 -- Ajoutez un attribut qui sera une collection d’acteurs
        self.listePersonnage = listePersonnage #8 --  Attribut acteur incarnant deux personnages


    def get_Titre(self):
        return self.titre

    def set_Titre(self,titre):
        self.titre = titre

    def get_AnneeDeSortie(self):
        return self.anneeDeSortie

    def set_AnneeDeSortie(self,anneeDeSortie):
        self.anneeDeSortie = anneeDeSortie

    def get_NuméroEpisode(self):
        return self.numeroEpisode

    def set_NuméroEpisode(self,numeroEpisode):
        self.numeroEpisode = numeroEpisode

    def get_Cout(self):
        return self.cout

    def set_Cout(self,cout):
        self.cout = cout

    def get_Recette(self):
        return self.recette

    def set_Recette(self,recette):
        self.recette = recette

    def __str__(self):
        return "Film: " + self.titre +  " Annee De Sortie: " + str(self.anneeDeSortie) +  " Numéro Episode: " + str(self.numeroEpisode) +  " Cout: " + str(self.cout) +  " Recette: " + str(self.recette)

    def addActeurs(self, listActeur):
        for i in range(0, len(listActeur)):
            self.listActeur.append(listActeur[i])

    def nbActeurs(self): # 11) a) -- nbActeurs() qui vous retourne le nombre d’acteurs du film
        return len(self.listActeur)

    def nbPersonnages(self): # 11) b) -- nbPersonnages() le nombre de personnages de ce film
        return len(self.listePersonnage)

    def calculBenefice(self): # 11) c) -- calculBénéfice() qui retourne le montant du bénéfice et un booléen dans un duet pour savoir si le film est bénéficiaire et si oui, de combien
        if self.recette >= self.cout:
            return [self.recette - self.cout, True]
        else:
            return [self.cout - self.recette, False]

    def isBefore(self, date): # 11) c) isBefore(annee) qui retourne si True ou False le film est sortie avant une année passée en paramètre
        return date > self.anneeSortie

    def tri(self): # 12 -- d’une méthode tri() de la classe FILM qui trie les acteurs par ordre alphabétique dans la collection
        return self.acteurs.sort(key=lambda x: x.nom)

    def backup(self): # 12 -- d’une méthode tri() de la classe FILM qui trie les acteurs par ordre alphabétique dans la collection
        return self.anneeDeSortie, self.titre, self.calculBenefice

"""
    def calculBénéfice(self):
        x = self.recette - self.cout
        if  x > 0:
            return x,"Le film est bénéficiaire "
        else:
            print("Le film est déficitaire")
"""



