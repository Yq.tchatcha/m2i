from Ville import *
from Capitale import *

v1 = Ville("Toulouse")
v2 = Ville("Strasbourg",272975)

print(v1)
print(v2)


c1 = Capitale("Paris","France")
c2 = Capitale("Rome","Italie",2700000)

c1.set_NbHabitants(2181371)


print(c1)
print(c2)

print("catégorie de la ville de " + v1.get_Nom() + " : " + v1.categorie())

print("catégorie de la ville de " + v2.get_Nom() + " : " + v2.categorie())

print("catégorie de la ville de " + c1.get_Nom() + " : " + c1.categorie())
